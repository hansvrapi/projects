# Spam Account Analysis

## Approach

1. Derive new data from the given log: add the following information for each user: 

- userid: unique identifier for each user in the log 
- receiver_nr: total number of users that receive a message from the user
- msg_nr: total number of messages the user has sent
- time_online: overall time in seconds that the user has been online since logging the activities
- min_time_between_msg: minimum time between two messages from the user (given in seconds)
- max_time_between_msg: max time between two messages from the user (given in seconds)
- min_nr_of_msg: minimal number of messages the sender has sent to some user
- max_nr_of_msg: maximal number of messages the sender has sent to some user
- avg_nr_of_msg: average number of messages the sender has sent to some user
- msg_nr_x_percentile: the x-percentile of the number of messages the sender has sent to some other user for x in {25,50,75}
    
2. Take a closer look at derived data (plot correlations, value distribution) 

3. Cluster the data based on a subset of the derived features to find similarities

4. Use the observations from point 2 to derive a subset of bot accounts: receiver_nr == 163 or receiver_nr == 162 - all of these users - except one - come from one cluster. That one user doesn't seem to be a bot. Take a look at other users that are in the same cluster to find other bots.

5. Identify accounts that are suspicious and classify them as bot accounts: 
    - users that send only one message to a high number of users
    - users that mostly (except in a few cases) send the same number of messages (>2) to many other users: we use percentiles here

6. Analyse users where min_time_between_msg == 0: 
     - it is okay if they send the same message to different users due to groups
     - check whether there are users that send two messages to the same person within a second: not possible to deduct anything since theoretically, this could be possible (e.g. if the message consists of one letter)
     - check here are users that send more than two messages to the same person within a second: these users would likely be bots and they are actually considered in the above cases.
