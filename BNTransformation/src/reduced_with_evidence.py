import stormpy
from stormpy.storage.storage import JaniModel
from stormpy.storage.storage import SparseDtmc
import re
from typing import List, Set, Dict, Tuple, Optional




def find_state_ids_with_evidence(jani_program: JaniModel, evidence: str) -> ([int], int):
    '''

    :param jani_program: a JaniModel in which the evidence is searched
    :param evidence: string with evidence in the form : 'Mood=1 Grade=0 ...'
    :return: tuple containing list of ids of states with evidence and probability of evidence
    '''

    formula_str = f"P=? [F {evidence}]"
    properties = stormpy.parse_properties_for_jani_model(formula_str, jani_program)
    model = stormpy.build_model(jani_program, properties)
    result = stormpy.model_checking(model, properties[0])

    initial_state = model.initial_states[0]
    prob_of_evidence = result.at(initial_state)
    states_with_evidence = list(filter(lambda state : result.at(state) == 1.0, model.states))
    state_ids_with_evidence = [state.id for state in states_with_evidence]
    return state_ids_with_evidence, prob_of_evidence


def find_final_state_ids(model: SparseDtmc) -> List[int]:
    '''

    :param model: Jani model which contains the final states
    :return: List of ids of final states in the model
    '''
    final_states = []
    for state in model.states:
        for action in state.actions:
            if len(action.transitions) == 1 :
                for t in action.transitions:
                    # only transition is a self loop in final states
                    if t.value() == 1 and t.column == state.id:
                        final_states.append(state.id)
    return final_states

def valuation_as_dict(valuation: str) -> Dict[str,str]:
    '''

    :param valuation: string describing variable valuation
    :return: dictionary of variables and their values
    '''
    val_split = re.split('&|\t| ',valuation[1:-1])
    val_filtered = list(filter(lambda x : len(x)>0, val_split))
    val_dict_preparated =  [x.split('=') for x in val_filtered]
    val_dict_preparated_filtered = list(filter(lambda x : len(x)>1, val_dict_preparated))
    val_dict = dict(val_dict_preparated_filtered)

    return val_dict

def combine_mpe_dicts(dict_list: List[Dict[str,str]]) -> Dict[str,str]:
    combined_dict = {}
    for d in dict_list:
        for d_key, d_val in d.items():
            if (int(d_val) != -1):
                combined_dict[d_key] = d_val
    return combined_dict