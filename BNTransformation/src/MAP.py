# example call: python3 -m src.MAP JANI_data/studentMood_non_parametric.jani Grade=1 --subset Mood
# call from folder BNToJaniTransformation

import stormpy
from stormpy.utility import ShortestPathsGenerator
import argparse
from typing import List, Set, Dict, Tuple, Optional
from src.treelike_mc import *

def compute_map(jani_file_path: str, evidence: str, subset: List[str]) -> (Dict[str, str], float):
    '''
    computes maximum aposteriori probability (MAP) for given jani file and given evidence and given subset
    :param jani_file_path:  string containing path to jani file
    :param evidence: string with evidence in the form : 'Mood=1 Grade=0 ...'
    : param subset: list of strings containing the subset of variables we are interested in
    :return: tuple containing map in a dictionary and probability of map
    '''

    jani_program, jani_prop = stormpy.parse_jani_model(jani_file_path)
    states_with_evidence, prob_of_evidence = find_state_ids_with_evidence(jani_program,evidence)
    formula_str = f"P=? [F true]"
    properties = stormpy.parse_properties_for_jani_model(formula_str, jani_program)
    options = stormpy.BuilderOptions([p.raw_formula for p in properties])
    options.set_build_state_valuations()
    model = stormpy.build_sparse_model_with_options(jani_program, options)
    final_states = find_final_state_ids(model)
    # search MAP
    map_candidates = {} # encoded_valuation_string : probability
    spg = ShortestPathsGenerator(model, final_states)
    for k in range(1,len(final_states)+1):
        # temp_path is ordered by probability
        temp_path = spg.get_path_as_list(k)
        # consider only final states, that can be reached from states with evidence
        if len(set(temp_path).intersection(set(states_with_evidence))) == 0:
            continue
        else:
            path = spg.get_path_as_list(k)
            prob_of_path = spg.get_distance(k)
            # get valuation
            map_candidate_valuation = model.state_valuations.get_string(path[0])
            map_candidate_dict = valuation_as_dict(map_candidate_valuation)

            # encode valuation of subset by identifier tuple
            value_list = []
            for var in subset:
                value_list.append(map_candidate_dict[var])
            identifier_tuple = tuple(value_list)
            # update probability of identifier tuple
            if identifier_tuple in map_candidates:
                map_candidates[identifier_tuple] += prob_of_path
            else:
                map_candidates[identifier_tuple] = prob_of_path

    # the MAP is the valuation of subset (encoded by identifier tuple), which has the highest probability
    map_identifier_tuple = max(map_candidates, key=map_candidates.get)
    map = dict(list(zip(subset,map_identifier_tuple)))
    probability = map_candidates[map_identifier_tuple]/prob_of_evidence

    return map, probability

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process path to Jani-File and Evidence')
    parser.add_argument('path', help='path to jani-file', type=str)
    parser.add_argument('evidence', nargs='+',
                        help='evidence in the form:  "Mood=1 Grade=1 ..."', type=str)
    parser.add_argument('--subset', help = 'subset of variables for which we are searching maximum aposteriori in the form: Grade,Mood,Difficulty,...', type=str)
    args = parser.parse_args()
    # build evidence from arguments
    file_path = args.path
    evidence = ' & '.join(args.evidence)
    subset = args.subset.split(',')
    map, probability = compute_map(file_path, evidence, subset)
    print(f"\nMAP is {map} \nwith probability {probability}.")

