# example call: python3 -m src.MPE JANI_data/studentMood_non_parametric.jani Grade=1
# call from folder BNToJaniTransformation

import stormpy
from stormpy.utility import ShortestPathsGenerator
from stormpy.storage.storage import JaniModel
from stormpy.storage.storage import SparseDtmc
import argparse
from typing import List, Set, Dict, Tuple, Optional
from src.treelike_mc import *

def compute_mpe(jani_file_path: str, evidence: str) -> (Dict[str,str], float):
    '''
    computes most probable explanation (MPE) for given jani file and given evidence
    :param jani_file_path:  string containing path to jani file
    :param evidence: string with evidence in the form : 'Mood=1 Grade=0 ...'
    :return: tuple containing mpe in a dictionary and probability of mpe
    '''

    jani_program, jani_prop = stormpy.parse_jani_model(jani_file_path)
    states_with_evidence, prob_of_evidence = find_state_ids_with_evidence(jani_program,evidence)
    formula_str = f"P=? [F true]"
    properties = stormpy.parse_properties_for_jani_model(formula_str, jani_program)
    options = stormpy.BuilderOptions([p.raw_formula for p in properties])
    options.set_build_state_valuations()
    model = stormpy.build_sparse_model_with_options(jani_program, options)
    final_states = find_final_state_ids(model)
    # search MPE
    prob_of_path = 0
    mpe_dict = {}
    spg = ShortestPathsGenerator(model, final_states)
    for k in range(1,len(final_states)+1):
        # temp_path is ordered by probability
        temp_path = spg.get_path_as_list(k)
        # consider only final states, that can be reached from states with evidence
        if len(set(temp_path).intersection(set(states_with_evidence))) == 0:
            continue
        else:
            # first reachable path is most probable one
            path = spg.get_path_as_list(k)
            prob_of_path = spg.get_distance(k)
            mpe_valuation = model.state_valuations.get_string(path[0])
            mpe_dict = valuation_as_dict(mpe_valuation)
            break
    # compute probability of MPE
    probability = prob_of_path / prob_of_evidence
    return mpe_dict, probability

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process path to Jani-File and Evidence')
    parser.add_argument('path', help='path to jani-file', type=str)
    parser.add_argument('evidence', nargs='+',
                        help='evidence in the form:  "Mood=1 Grade=1 ..."', type=str)
    args = parser.parse_args()
    # build evidence from arguments
    file_path = args.path
    evidence = ' & '.join(args.evidence)
    mpe, probability = compute_mpe(file_path, evidence)
    print(f"\nMPE is {mpe} \nwith probability {probability}.")

