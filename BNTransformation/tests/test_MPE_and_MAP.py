#!/usr/bin/env python3
# call in folder NBtoJaniTransformation: python3 -m tests.test_MPE

import unittest
#from .context import compute_mpe, compute_map
from src.MPE import compute_mpe
from src.MAP import compute_map
class TestMpe(unittest.TestCase):
    test_cases = {
        'weather_1': {'file_path': 'test_data/weather.jani', 'evidence': 'Sun=1 & Daytime=1 & Wind=1',
                      'mpe': {'Rain': '1'}, 'prob': 0.65},
        'weather_2': {'file_path': 'test_data/weather.jani', 'evidence': 'Sun=0 & Daytime=0 & Rain=1',
                      'mpe': {'Wind': '0'}, 'prob': 0.775862069},
        'weather_3': {'file_path': 'test_data/weather.jani', 'evidence': 'Sun=0 & Wind=0 & Rain=1',
                      'mpe': {'Daytime': '0', 'Sun': '0', 'Wind': '0', 'Rain' : '1'}, 'prob': 0.75},
        'weather_4': {'file_path': 'test_data/weather.jani', 'evidence': 'Daytime=0 & Sun=0 & Wind=0 & Rain=1',
                      'mpe': {'Daytime': '0', 'Sun': '0', 'Wind': '0', 'Rain': '1'}, 'prob': 1},
        'one_1': {'file_path': 'test_data/one.jani', 'evidence': 'One=1',
                      'mpe': {'One': '1'}, 'prob': 1},
        'one_2': {'file_path': 'test_data/one.jani', 'evidence': 'true',
                'mpe': {'One': '1'}, 'prob': 0.51}
    }
    def helper_mpe(self, data):
        mpe_dict, prob = compute_mpe(data['file_path'], data['evidence'])
        for var in data['mpe']:
            self.assertEqual(mpe_dict[var], data['mpe'][var], f'{var} should have value {data["mpe"][var]}.')
        self.assertAlmostEqual(prob, data['prob'])

    def test_mpe_weather_1(self):
        data = self.test_cases['weather_1']
        self.helper_mpe(data)

    def test_mpe_weather_2(self):
        data = self.test_cases['weather_2']
        self.helper_mpe(data)

    def test_mpe_weather_3(self):
        data = self.test_cases['weather_3']
        self.helper_mpe(data)

    def test_mpe_weather_4(self):
        data = self.test_cases['weather_4']
        self.helper_mpe(data)

    def test_mpe_one_1(self):
        data = self.test_cases['one_1']
        self.helper_mpe(data)

    def test_mpe_one_2(self):
        data = self.test_cases['one_2']
        self.helper_mpe(data)


class TestMap(unittest.TestCase):
    test_cases = {
        'one_1': {'file_path': 'test_data/one.jani', 'evidence': 'true', 'subset': ['One'],
                      'map': {'One': '1'}, 'prob': 0.51},
        'one_2': {'file_path': 'test_data/one.jani', 'evidence': 'One=1', 'subset': ['One'],
                      'map': {'One': '1'}, 'prob': 1},
        'weather_1': {'file_path': 'test_data/weather.jani', 'evidence': 'Daytime=1', 'subset': ['Sun', 'Wind'],
                      'map': {'Sun': '1', 'Wind': '0'}, 'prob': 0.42},
        'weather_2': {'file_path': 'test_data/weather.jani', 'evidence': 'Daytime=1', 'subset': ['Daytime','Sun', 'Wind'],
                      'map': {'Daytime': '1', 'Sun': '1', 'Wind': '0'}, 'prob': 0.42},
        'weather_3': {'file_path': 'test_data/weather.jani', 'evidence': 'Sun=0',
                      'subset': ['Rain'],
                      'map': {'Rain': '0'}, 'prob': 0.59}
    }
    def test_map_one_1(self):
        data = self.test_cases['one_1']
        self.helper_map(data)
    def test_map_one_2(self):
        data = self.test_cases['one_2']
        self.helper_map(data)

    def test_map_weather_1(self):
        data = self.test_cases['weather_1']
        self.helper_map(data)
    def test_map_weather_2(self):
        data = self.test_cases['weather_2']
        self.helper_map(data)
    def test_map_weather_3(self):
        data = self.test_cases['weather_3']
        self.helper_map(data)

    def helper_map(self, data):
        map_dict, prob = compute_map(data['file_path'], data['evidence'],data['subset'])
        for var in data['map']:
            self.assertEqual(map_dict[var], data['map'][var], f'{var} should have value {data["map"][var]}.')
        self.assertAlmostEqual(prob, data['prob'])


if __name__ == '__main__':
    unittest.main()